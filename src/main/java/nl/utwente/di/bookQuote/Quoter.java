package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    HashMap<String,Double> hashMap=new HashMap<>();

    public Quoter() {
        hashMap.put("1",10.0);
        hashMap.put("2",45.0);
        hashMap.put("3",20.0);
        hashMap.put("4",35.0);
        hashMap.put("5",50.0);
    }

    double getBookPrice(String isbn){
       return hashMap.get(isbn);
    }
}
